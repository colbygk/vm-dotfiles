set nocompatible      " We're running Vim, not Vi!

set background=dark
set cindent shiftwidth=2

set shiftwidth=2
set expandtab
set tabstop=2
set softtabstop=2
set ai

syntax on             " Enable syntax highlighting
filetype on           " Enable filetype detection
filetype indent off    " Enable filetype-specific indenting
"set nocindent
"set nosmartindent
"set noautoindent
"set indentexpr=""
filetype plugin on    " Enable filetype-specific plugins

" source ~/.vim.d/python.vim

"
" compiler ruby         " Enable compiler support for ruby



" Under 10.7 Lion, 256 colors in the norm
set t_Co=256
colorscheme mustang
"colorscheme desert

nnoremap <Leader>p execute ":!hwk pdf<CR>"
map! ,fr \frac{}{}<ESC>ba
map! ,pd \pd{}{}<ESC>ba
map! ,que question do<ENTER>end<ESC>O  text %q(<RETURN>)<ESC>O 
map! ,sec section do<ENTER>end<ESC>O  text %q(<RETURN>)<ESC>O 
map! ,sol solution %q(<ENTER>)<ESC>O
map! ,par part %q(<RETURN>) do<RETURN>end<ESC>kO
map! ,smc \smac{<RETURN>}<ESC>O
map! ,vec \vec{}<ESC>ba
map! ,txt \text{}<ESC>ba

source $VIMRUNTIME/menu.vim
