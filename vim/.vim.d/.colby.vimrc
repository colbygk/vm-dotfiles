set background=dark
set cindent shiftwidth=2

set shiftwidth=2
set expandtab
set tabstop=2

set nocompatible      " We're running Vim, not Vi!
syntax on             " Enable syntax highlighting
filetype on           " Enable filetype detection
filetype indent on    " Enable filetype-specific indenting
filetype plugin on    " Enable filetype-specific plugins

source ~/.vim.d/python.vim

"
" compiler ruby         " Enable compiler support for ruby



" Under 10.7 Lion, 256 colors in the norm
set t_Co=256
colorscheme mustang
"colorscheme desert
