" Vim syntax file
" hwk 
" Colby Gutierrez-Kraybill
"

if exists("b:current_syntax")
  finish
endif

set nocindent
set nosmartindent
set noautoindent
set indentexpr=""

set ai

let b:current_syntax = "hwk"

syn keyword basicLanguageKeywords section part title author_name due_date class_name class_professor class_name class_time question text solution
syn keyword basicBlocks do end

" Expression Substitution and Backslash Notation
syn match rubyStringEscape "\\\\\|\\[abefnrstv]\|\\\o\{1,3}\|\\x\x\{1,2}"               contained display
syn match rubyStringEscape "\%(\\M-\\C-\|\\C-\\M-\|\\M-\\c\|\\c\\M-\|\\c\|\\C-\|\\M-\)\%(\\\o\{1,3}\|\\x\x\{1,2}\|\\\=\S\)" contained display
syn match rubyQuoteEscape  "\\[\\']"                          contained display


" Normal String and Shell Command Output
syn region rubyString matchgroup=rubyStringDelimiter start="\"" end="\"" skip="\\\\\|\\\"" contains=@rubyStringSpecial fold
syn region rubyString matchgroup=rubyStringDelimiter start="'"  end="'"  skip="\\\\\|\\'"  contains=rubyQuoteEscape    fold
syn region rubyString matchgroup=rubyStringDelimiter start="`"  end="`"  skip="\\\\\|\\`"  contains=@rubyStringSpecial fold

syn match rubyDelimEscape "\\[(<{\[)>}\]]" transparent display contained contains=NONE

syn region rubyNestedParentheses    start="("  skip="\\\\\|\\)"  matchgroup=rubyString end=")"  transparent contained
syn region rubyNestedCurlyBraces    start="{"  skip="\\\\\|\\}"  matchgroup=rubyString end="}"  transparent contained
syn region rubyNestedAngleBrackets  start="<"  skip="\\\\\|\\>"  matchgroup=rubyString end=">"  transparent contained
syn region rubyNestedSquareBrackets start="\[" skip="\\\\\|\\\]" matchgroup=rubyString end="\]" transparent contained

" Generalized Single Quoted String, Symbol and Array of Strings
syn region rubyString matchgroup=rubyStringDelimiter start="%[qw]\z([~`!@#$%^&*_\-+=|\:;"',.?/]\)" end="\z1" skip="\\\\\|\\\z1" fold
syn region rubyString matchgroup=rubyStringDelimiter start="%[qw]{"          end="}"   skip="\\\\\|\\}" fold contains=rubyNestedCurlyBraces,rubyDelimEscape
syn region rubyString matchgroup=rubyStringDelimiter start="%[qw]<"          end=">"   skip="\\\\\|\\>" fold contains=rubyNestedAngleBrackets,rubyDelimEscape
syn region rubyString matchgroup=rubyStringDelimiter start="%[qw]\["           end="\]"  skip="\\\\\|\\\]"  fold contains=rubyNestedSquareBrackets,rubyDelimEscape
syn region rubyString matchgroup=rubyStringDelimiter start="%[qw]("          end=")"   skip="\\\\\|\\)" fold contains=rubyNestedParentheses,rubyDelimEscape
syn region rubySymbol matchgroup=rubySymbolDelimiter start="%[s]\z([~`!@#$%^&*_\-+=|\:;"',.?/]\)"  end="\z1" skip="\\\\\|\\\z1" fold
syn region rubySymbol matchgroup=rubySymbolDelimiter start="%[s]{"           end="}"   skip="\\\\\|\\}" fold contains=rubyNestedCurlyBraces,rubyDelimEscape
syn region rubySymbol matchgroup=rubySymbolDelimiter start="%[s]<"           end=">"   skip="\\\\\|\\>" fold contains=rubyNestedAngleBrackets,rubyDelimEscape
syn region rubySymbol matchgroup=rubySymbolDelimiter start="%[s]\["          end="\]"  skip="\\\\\|\\\]"  fold contains=rubyNestedSquareBrackets,rubyDelimEscape
syn region rubySymbol matchgroup=rubySymbolDelimiter start="%[s]("           end=")"   skip="\\\\\|\\)" fold contains=rubyNestedParentheses,rubyDelimEscape


syn region doBlock start='do' end='end' fold transparent contains=solution,part,text
syn region quoteBlock start='%q(' end=')' fold transparent
syn region regularQuote start='"' end='"'
hi def link quotedBlock Identifer
hi def link regularQuote Identifier
hi def link basicLanguageKeywords Constant
hi def link basicBlocks Statement

hi def link rubyQuoteEscape   rubyStringEscape
hi def link rubyStringEscape    Special
hi def link rubyInterpolationDelimiter  Delimiter
hi def link rubyNoInterpolation   rubyString
hi def link rubySharpBang   PreProc
hi def link rubyRegexpDelimiter   rubyStringDelimiter
hi def link rubySymbolDelimiter   rubyStringDelimiter
hi def link rubyStringDelimiter   Delimiter
hi def link rubyString      String

